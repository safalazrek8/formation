const sellerController=require('../controllers/sellerController')
const express =require("express")
//express pour les route et la creation de serveur
const route=express.Router()
const multer= require ("multer")
//to upload file add package multer with destination path
const upload =multer({dest:__dirname+'/upload/images'})

 route.post('/create',upload.single("avatar"),sellerController.create)
 
//  route.get('/getUsers',userController.getAllUserd)
//  route.get('/getone/:id',userController.getUserById)
//  route.delete('/delete/:id',userController.delete)
//  route.put('/update/:id',userController.update) 
//  route.post("/login",userController.authentificate)
 module.exports=route