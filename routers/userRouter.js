const userController=require('../controllers/userController')
const express =require('express')
//express pour les route et la creation de serveur
const route=express.Router()
route.post('/addUser',userController.createuser)
route.get('/getUsers',userController.getAllUserd)
route.get('/getone/:id',userController.getUserById)
route.delete('/delete/:id',userController.delete)
route.put('/update/:id',userController.update)
route.post("/login",userController.authentificate)
module.exports=route