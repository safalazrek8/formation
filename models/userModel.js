const mongoose=require ('mongoose')
const bcrypt =require('bcrypt')
const Schema=mongoose.Schema;
const userSchema = new Schema ({
    name :{
        type : String ,
        trim:true,
        requiquired:[true,"Name is required"]

    },
    prenon :{
        type : String ,
        trim:true,
        requiquired:[true,"Name is required"]

    },
    phone :{
        type : String ,
        trim:true,
        requiquired:[true,"Name is required"]

    },
    password :{
        type : String ,
        trim:true,
        requiquired:[true,"Name is required"]

    },
    email:{
        type:String,
        trim:true,
        requiquired:[true,"eamil is required"]
    },
})
//function asuynchrone permet de crypter le mp avant d'enregistrer dans la bd
.pre("save",function(next){
    this.password=bcrypt.hashSync(this.password,10)
    next()//save sans retour execution et passe
})
module.exports=mongoose.model('user',userSchema);